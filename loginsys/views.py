from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import auth
from loginsys.forms import UserCreationForm


def login(request):

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username = username, password = password)

        if user is not None:
            if user.is_active:
                auth.login(request, user)
                return redirect('/')
            else:
                login_error = 'Dissabled account'
                return render(request, 'login.html', locals())

        else:
            login_error = 'Uncorrect login or password'
            return render(request, 'login.html', locals(), status = 401)

    else:
        return render(request, 'login.html', locals())


def logout(request):
    print('asd;sad')
    auth.logout(request)
    return render(request, 'index.html', locals())


def registration(request):

    form = UserCreationForm()
    if request.method == 'POST':
        new_user = UserCreationForm(request.POST)
        if new_user.is_valid():
            new_user.save()
            user_login = auth.authenticate(username = new_user.cleaned_data['username'],
                                         password = new_user.cleaned_data['password1'])
            auth.login(request, user_login)
            return redirect('/')
        else:
            form = new_user
    return render(request, 'registration.html', locals())
